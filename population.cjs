let arr = require('./countries.json');

function population(arr) {
    return arr.reduce((acc, obj) => {
        let region = obj.region

        if(region && region === 'Europe'){
            acc += obj.population;
        }
        return acc;
    },0)
}
let countryData = population(arr);
console.log(countryData);