let arr = require('./countries.json');

function populationall(arr) {

    let EuropeRegion = arr.filter((obj) => obj.region === 'Europe');
    
    return EuropeRegion.reduce((acc,obj)=>{

        let subregion = obj.subregion;
        let population = obj.population;

        if(subregion in acc){
            acc[subregion] += population;
        }else{
            acc[subregion] = population;
        }
        return acc;
    },{})
}
let countryData = populationall(arr);
console.log(countryData);