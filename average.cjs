let arr = require('./countries.json');

function average(arr) {

    let allRegion = arr.filter((obj) => obj.region !== "");

    return allRegion.reduce((acc, obj) => {

        let subregion = obj.subregion;
        let population = obj.population;
        let region = obj.region;

        let count = numberOfRegion(subregion,allRegion);

        if (region in acc) {
            if (subregion in acc[region]) {
                acc[region][subregion] += population/count;
            } else {
                acc[region][subregion] = population/count;
            }
        } else {
            acc[region] = {};
            acc[region][subregion] = population/count;
        }
        return acc;
    }, {})
}
function numberOfRegion(subregion,arr){
    return arr.reduce((acc,obj)=>{
        if(obj.subregion === subregion){
            acc += 1;
        }
        return acc;
    },0);
}

let countryData = average(arr);
console.log(countryData);