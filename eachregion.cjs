let arr = require('./countries.json');

function eachRegion(arr) {

    let allRegion  = arr.filter((obj) => obj.region !== "");
    
    return allRegion.reduce((acc,obj)=>{

        let subregion = obj.subregion;
        let population = obj.population;
        let region = obj.region; 

        if(region in acc){
            if(subregion in acc[region]){
                acc[region][subregion]+=population
            }else{
                acc[region][subregion] = population;
            }
        }else{
            acc[region] = {};
            acc[region][subregion] = population;
        }
        return acc;
    },{})
}
let countryData = eachRegion(arr);
console.log(countryData);