let arr = require('./countries.json');

function countryName(arr) {
    return arr.reduce((acc, obj) => {

        if (obj.capital) {
            let countryName = obj.name.official
            let capital = obj.capital[0];

            acc[countryName] = capital;

        } else {
            acc[countryName] = " ";
        }

        return acc;
    }, {})
}
countryData = countryName(arr);
console.log(countryData);